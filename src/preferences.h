/*
    Copyright 2011 rhn <gihu.rhn@porcupinefactory.org>
    Copyright 2013 Paweł Czaplejewicz

    This file is part of Jazda.

    Jazda is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jazda is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Jazda.  If not, see <http://www.gnu.org/licenses/>.
*/

/* basic constants and switches, pin defines */

/* REQUIRES: none
*/

#include "boards/v0.9.1.h"

/* special options */
#define HIGH_PRECISION_CALCULATIONS // makes speed calculation 32-bit instead of truncating to 16
#define LONG_CALCULATIONS // makes calculations possible on longer data types
#define FRAC_BITS 10 // Fixed-point fraction for PULSE_DIST and SPEED_FACTOR. WARNING! modifying may lead to overflow errors and better precision (most likely just the former). Less then 8 is not recommended if LONG_SPEED is used

/* PREFERENCES */
// #define DEBUG

#define CRANK
#define CADENCE
#define STORAGE
#define BACKLIGHT
#define DISTANCE
#define CURRENT_SPEED
#define SPEED_VS_DISTANCE_PLOT
#define STOPWATCH
#define MAXSPEED
#define AVGSPEED
#define COMBINED_RESET
#define SPEED_VS_TIME_PLOT
#define CONFIG
// #define MOD_BACKLIGHT // backlight control via module
#define BACKLIGHT_BUTTON // backlight control via hw button
//#define BACKLIGHT_VOLTAGE // shows voltage along with backlight
#define LCD_CLEAN


#define METRIC_PULSE_DIST 2133L // millimeters
