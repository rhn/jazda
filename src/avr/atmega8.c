/*
    Copyright 2011 rhn <gihu.rhn@porcupinefactory.org>
    Copyright 2013 Paweł Czaplejewicz

    This file is part of Jazda.

    Jazda is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jazda is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Jazda.  If not, see <http://www.gnu.org/licenses/>.
*/

/** This file connects signals from `modules/signals.h`
 * and button handlers from `actions.h` to AtMega8 family hardware.
*/

#ifdef ATMEGA8

#define EINT_REG GICR
#define TIMER_REG TIMSK
#define TIMER_FLAG_REG TIFR
#define EINTC_REG MCUCR
#define SLEEP_REG MCUCR
#define V_BG_cV 130 // Centivolts of Vbg
#define BUTTON_MUX

#elif defined(ATMEGAX8)

#define EINT_REG EIMSK
#define TIMER_REG TIMSK1
#define TIMER_FLAG_REG TIFR1
#define EINTC_REG EICRA
#define SLEEP_REG SMCR
#define V_BG_cV 110 // Centivolts of Vbg

#else
#error "Unsupported AtMega type"
#endif


void eint_enable(uint8_t interrupt) {
    EINT_REG |= (1<<interrupt);
}

#include "avr.c"

#include "../actions.h"
#include "../signals.h"

#ifdef BUTTON_MUX
#define EMUINT0PIN LEFTPIN
#define EMUINT0PORT LEFTPORT
#define EMUINT0DIR LEFTDIR
void EMUINT0_handler(uint8_t state) {
    on_left_button(state);
}

#define EMUINT1PIN RIGHTPIN
#define EMUINT1PORT RIGHTPORT
#define EMUINT1DIR RIGHTDIR
void EMUINT1_handler(uint8_t state) {
    on_right_button(state);
}

#define EMUINT2PIN SELECTPIN
#define EMUINT2PORT SELECTPORT
#define EMUINT2DIR SELECTDIR
void EMUINT2_handler(uint8_t state) {
    on_select_button(state);
}

#ifdef CRANK
    #define EMUINT3PIN CRANKPIN
    #define EMUINT3PORT CRANKPORT
    #define EMUINT3DIR CRANKDIR
    void EMUINT3_handler(uint8_t state) {
        if (!state) {
            on_crank_pulse();
        }
    }
#endif

#ifdef BACKLIGHT_BUTTON
    #define EMUINT4PIN BACKLIGHTBTNPIN
    #define EMUINT4PORT BACKLIGHTBTNPORT
    #define EMUINT4DIR BACKLIGHTBTNDIR
    void EMUINT4_handler(uint8_t state) {
        on_backlight_button(state);
    }
#endif

#include "emuint.c"
#endif

void setup_cpu(void) {
   // makes CPU clock 1 MHz
   // can't be done on this device
}

/* initializes adc with no interrupts */
void adc_init(void) {
    // use Vcc as reference
    ADMUX |= (1 << REFS0);
    ADMUX &= ~(1 << REFS1);
    // select Vbg as voltage
    ADMUX |= (1 << MUX3) | (1 << MUX2) | (1 << MUX1);
    ADMUX &= ~(1 << MUX0);
}

/* turns on adc */
void adc_enable(void) {
    ADCSRA |= (1 << ADEN);
}

/* turns off adc */
void adc_disable(void) {
    ADCSRA &= ~(1 << ADEN);
}

/* blocking read */

uint16_t adc_read(int8_t channel) {
    uint8_t mux_bits = 0xf; // 4 bits select the channel
    uint8_t channel_masked = ((uint8_t)channel) & mux_bits; // writing unnormalized value could clobber other bits in the register

    // Request a measurement
    ADMUX |= channel_masked << MUX0;
    ADCSRA |= (1 << ADSC);

    // Wait for measurement
    while (ADCSRA & (1<<ADSC)) {}

    return ADC;
}

/* blocking voltage measurement. returns reading in units of 10mV. Resolution: 40mV.
TODO: think about better return units */
uint16_t get_battery_voltage(void) {
    adc_enable();
    uint16_t raw_reading = adc_read(ADC_CHAN_V_BG); // Vbg source @ 1.1-1.3V
    adc_disable();
    
    // R - reading
    // V_{BG} - pre-calibrated source
    // V_{CC} - battery, reference
    // R = {V_{BG}\cdot1024}\over{V_{CC}}
    // V_{CC} = {V_{BG}\cdot1024}\overR
    if (raw_reading == 0) {
        return 0;
    }
    
    // split precision into 2 parts: 1x0cV, 10bits resolution
    // apply 8 bits first to get 33280 (fitting in uint16_t)
    // apply 2 bits later to normalize to 10bits
    uint16_t voltage = ((V_BG_cV << 8) / raw_reading) << 2;
    return voltage;
}
