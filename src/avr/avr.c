/*
    Copyright 2011 rhn <gihu.rhn@porcupinefactory.org>

    This file is part of Jazda.

    Jazda is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Jazda is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Jazda.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <avr/interrupt.h>
#include <avr/eeprom.h> 

/* AVR-specific common routines.
This module links signals from signals.h to interrupts in generic AVR hardware.

REQUIRES:
AVR #defines regarding interrupts
Signals

TODO: chip-specific functions to chip-specific files
FIXME: race condition in reading extended_time while overflow should be
happening.
*/

#include "../signals.h"
#include "../lib/timer.h"

#define ADC_CHAN_V_BG (-2)

volatile uint16_t extended_time = 0;

void setup_wheel_pulse(void) {
  /* -----  hwint button */  
  PULSEDIR &= ~(1<<PULSEPIN); /* set PD2 to input */
  PULSEPORT |= 1<<PULSEPIN; // enable pullup resistor

  // interrupt on INT0 pin falling edge (sensor triggered)
  EINTC_REG|= (1<<ISC01);
  EINTC_REG &= ~(1<<ISC00);
  /* ------ end */

  // turn on interrupts!
  eint_enable(INT0);
}

ISR(INT0_vect) {
    on_wheel_pulse();
}

#ifdef BUTTON_MUX
void emuint_dispatch(void);

ISR(INT1_vect) {
  emuint_dispatch();
}
#else

volatile uint8_t latest_buttons = 0;

uint8_t read_buttons(void) {
  // Assume all buttons share the same port
  return LEFTPORT & (_BV(LEFTPIN) | _BV(RIGHTPIN) | _BV(SELECTPIN) | _BV(BACKLIGHTBTNPIN));
}

#include "../actions.h"

ISR(PCINT2_vect) {
  uint8_t previous_buttons = latest_buttons;
  latest_buttons = read_buttons();
  uint8_t current_buttons = latest_buttons; // Copy to stop referring to the volatile one
  
  // TODO: the following part is very generic, should be put in a non-AVR file
  uint8_t button_changes = previous_buttons ^ current_buttons;

  for (uint8_t i = 0; i < 8; i++) {
    if (_BV(i) & button_changes) {
      uint8_t buttonreleased = _BV(i) & current_buttons; // 1 = released
      switch (i) {
        case LEFTPIN: {
          on_left_button(buttonreleased);
          break;
        } case RIGHTPIN: {
          on_right_button(buttonreleased);
          break;
        } case SELECTPIN: {
          on_select_button(buttonreleased);
          break;
        }
#ifdef CRANK
        case CRANKPIN: {
          if (!buttonreleased) {
            on_crank_pulse();
          }
          break;
        }
#endif
#ifdef BACKLIGHT_BUTTON
        case BACKLIGHTBTNPIN: {
          on_backlight_button(buttonreleased);
          break;
        }
#endif
        default: {
          // uhh... crash or something?
        }
      }
    }
  }
}

#endif

void setup_buttons(void) {
  LEFTDIR &= ~(1<<LEFTPIN);
  LEFTPULLUP |= 1<<LEFTPIN;

  RIGHTDIR &= ~(1<<RIGHTPIN);
  RIGHTPULLUP |= 1<<RIGHTPIN;
  
  SELECTDIR &= ~(1<<SELECTPIN);
  SELECTPULLUP |= 1<<SELECTPIN;
  
  #ifdef CRANK
      CRANKDIR &= ~(1<<CRANKPIN);
      CRANKPULLUP |= 1<<CRANKPIN;
  #endif
  
  #ifdef BACKLIGHT_BUTTON
     BACKLIGHTBTNDIR &= ~(1<<BACKLIGHTBTNPIN);
     BACKLIGHTBTNPULLUP |= 1<<BACKLIGHTBTNPIN;
  #endif
  
  #ifdef BUTTON_MUX
    BUTTONDIR &= ~(1<<BUTTONPIN);
    BUTTONPORT |= 1<<BUTTONPIN;
  
    // interrupt on INT1 pin both edges (emuint state change)
    MCUCR |= (1<<ISC10);
    MCUCR &= ~(1<<ISC11);

    eint_enable(INT1);
  #else
    PCMSK2 |= _BV(LEFTINT) | _BV(RIGHTINT) | _BV(CRANKINT) | _BV(BACKLIGHTBTNINT) | _BV(SELECTINT);
    
    latest_buttons = read_buttons();
    
    PCICR |= 1<<PCIE2;
  #endif
}

volatile uint8_t pulses = 0;


void setup_timer(void) {
// sets up millisecond precision 16-bit timer
/* TODO: consider using 8-bit timer overflowing every (half) second + 4-bit USI*/
// set up prescaler to /64

/* PRESCALER SETTINGS:
CS12 CS11 CS10 Description
 0    0    0   No clock source. (Timer/Counter stopped)
 0    0    1   clkIO/1 (No prescaling)
 0    1    0   clkIO/8 (From prescaler)
 0    1    1   clkIO/64 (From prescaler)
 1    0    0   clkIO/256 (From prescaler)
 1    0    1   clkIO/1024 (From prescaler)
 1    1    0   External clock source on T1 pin. Clock on falling edge.
 1    1    1   External clock source on T1 pin. Clock on rising edge.
*/
  uint8_t clksrc;
  clksrc = TCCR1B;
// for /1024
//  clksrc |= 1<<CS12 | 1<<CS10;
//  clksrc &= ~(1<<CS11);
// for /64
//  clksrc |= 1<<CS11 | 1<<CS10;
//  clksrc &= ~(1<<CS12);
// for /256
  clksrc |= 1<<CS12;
  clksrc &= ~(1<<CS11) & ~(1<<CS10);

  TCCR1B = clksrc;

  TIMER_REG |= 1 << TOIE1; // enable overflow interrupt
  
  // set up each second timer
  OCR1B = TCNT1 + ONE_SECOND;
  TIMER_REG |= 1 << OCIE1B;
}

void set_immediate_trigger(void) {
  // FIXME: does it not trigger an interrupt instantly?
  // FIXME: does it do anything at all?
  TIMER_FLAG_REG |= 1 << OCF1A;
}

inline uint16_t get_time(void) {
  return TCNT1;
}

uint32_t get_extended_time() {
  return (((uint32_t)extended_time) << 16) | TCNT1; // TODO: may be possible to optimize in asm
}

void set_trigger_time(const uint16_t time) { // XXX: optimize: inline
  OCR1A = time;
  TIMER_REG |= 1 << OCIE1A;
}

void clear_trigger() {
  TIMER_REG &= ~(1 << OCIE1A);
}

ISR(TIMER1_COMPA_vect) {
    timer_dispatch();
}

ISR(TIMER1_OVF_vect) {
    extended_time++;
}

ISR(TIMER1_COMPB_vect) {
    on_each_second();
    OCR1B += ONE_SECOND;
}
