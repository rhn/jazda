/* Defines hardware configuration matching the v0.9.1 board. */

// TODO: define the CPU

// - Inputs

#define PULSEDIR DDRD
#define PULSEPORT PORTD
#define PULSEPULLUP PIND
#define PULSEPIN PD2

#define CRANKDIR DDRD
#define CRANKPORT PIND
#define CRANKPULLUP PORTD
#define CRANKPIN PD3
#define CRANKINT PCINT19

// -- Button interrupt - FIXME, interrupt circuit not present on the board any longer

#define BUTTONDIR DDRB
#define BUTTONPORT PORTB
#define BUTTONPULLUP PINB
#define BUTTONPIN PB0

// -- Buttons

#define LEFTPIN PD7
#define LEFTPORT PIND
#define LEFTPULLUP PORTD
#define LEFTDIR DDRD
#define LEFTINT PCINT23

#define RIGHTPIN PD5
#define RIGHTPORT PIND
#define RIGHTPULLUP PORTD
#define RIGHTDIR DDRD
#define RIGHTINT PCINT21

#define SELECTPIN PD6
#define SELECTPORT PIND
#define SELECTPULLUP PORTD
#define SELECTDIR DDRD
#define SELECTINT PCINT22

#define BACKLIGHTBTNPIN PD4
#define BACKLIGHTBTNPORT PIND
#define BACKLIGHTBTNDIR DDRD
#define BACKLIGHTBTNPULLUP PORTD
#define BACKLIGHTBTNINT PCINT20

// - Output
// -- Screen

#define CLKDIR DDRC
#define CLKPORT PORTC
#define CLKPIN PC4

#define SDADIR DDRC
#define SDAPORT PORTC
#define SDAPIN PC3

#define D_CDIR DDRC
#define D_CPORT PORTC
#define D_CPIN PC2

#define SCEDIR DDRC
#define SCEPORT PORTC
#define SCEPIN PC0
   
#define RSTDIR DDRC
#define RSTPORT PORTC
#define RSTPIN PC1

// -- Backlight
#define BACKLIGHTDIR DDRC
#define BACKLIGHTPORT PORTC
#define BACKLIGHTPIN PC5

#define ONE_SECOND 3695 // in timer ticks, calibrated for 1MHz/256 // TODO: autocalculate
