Jazda
=====

Jazda is a bicycle computer based on AVR microcontrollers and old mobile phone screens. The project covers both the software and hardware design, providing the source code and schematics.

Originally, it was built around AtTiny2313, now it's AtMega8. The microcontrollers were chosen because of their popularity and resistance to damage.

- [Project page](https://gitlab.com/rhn/jazda)
- [Project wiki](https://github.com/rhn/jazda/wiki/)
- [Introduction, tutorial and hardware documents](http://rhn.github.com/jazda/)

Features
--------

- 5 speed measuring functions: current, max, average, etc.
- Cadence measurement
- Plotting as you ride
- Low power consumption
- DIY hardware: breadboardable!

Assembly
--------

For hardware needed to assemble the device, see `docs/hardware`.

The schematics and layyout are available in Kicad format in the `kicad` directory, AND on the project page (PNG format).

You will need an AVR programmer to upload the software to the AtMega chip!

Prebuilt
--------

If you're not much into software, you can download the file from the [CI pipelines](https://gitlab.com/rhn/jazda/-/jobs). Look for artifacts:

![The artifacts button](https://gitlab.com/rhn/jazda/wikis/uploads/bd5bac8624aee059536b84ce8058f2cd/image.png)

Software building
-----------------

So far, only tested under Linux.

To build & upload the software, you will need:

- avr-gcc
- avr-binutils
- avr-libc
- avrdude
- [tup](http://gittup.org/tup/index.html)

Run the command:

```
sh ./build.sh flash atmega8
```

The results will end up in the `build-atmega8` directory. The hex file is going to be named `jazda-atmega8.hex`. Other files, useful for debugging, will be created under the names starting with `main-atmega8.`.

### Uploading

The `jazda-atmega8.hex` file contains the resulting program that should be placed on the microcontroller.

The provided `build.sh` file uses avrdude to achieve that.

1. Connect the AtMega8 chip to the programmer (pay attention to the orientation)
2. Connect the programmer to your computer (refer to the programmer's manual)
3. Consult avrdude manual to find your programmer's name (in this example, it's "usbasp")
4. Issue the commands:

```
sh ./flash.sh m8 usbasp
```

replacing `usbasp` with your programmer's name, and `m8` with your microcontroller's name. Find them using avrdude:

```
avrdude -c help
avrdude -p help
```

Once you flash the microcontroller successfully, remove it from the programmer (be careful to power down the programmer correctly), and plug it into the Jazda PCB. Plug in the battery, and you're ready to go!

Development
-----------

See the `HACKING.md` file.

Contact
-------

If you need help putting together the actual device, please send an email to jazda@librelist.com , or [come to chat](https://webchat.freenode.net/?channels=jazda) on #jazda IRC channel on [freenode](https://freenode.net/).

If you have any kind of feedback, file an [issue](https://gitlab.com/rhn/jazda/issues). If you are a knowledgeable and helpful person, you could try to reach the maintainer via email at gihu.rhn at porcupinefactory dot org.
