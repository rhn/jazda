Developing the bike computer
============================

Thank you for taking the interest in developing Jazda! I hope you submit your improvement to the repo: do [send a MR](https://gitlab.com/rhn/jazda/merge_requests), or send an email to jazda@librelist.com with your patch ([use `git format-patch`](https://thoughtbot.com/blog/send-a-patch-to-someone-using-git-format-patch)).

Software
--------

To start software development, you should get familiar with [tup](http://gittup.org/tup/). Inspect the `build.sh` script, or try the following commands:

```
cd jazda
tup init
tup variant config/atmega8.config
tup build-atmega8/main-atmega8.asm
tup
```

To flash, use *avrdude*:

```
avrdude -c usbasp -p m8 -U "flash:w:build-atmega8/jazda-atmega8.hex"
```

Hardware
--------

The hardware is being developed in [Kicad](http://www.kicad-pcb.org/). Open the `kicad/licznik.pro` file, and begin!

