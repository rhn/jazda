#!/bin/sh
set -e

# Builds and flashes Jazda using avrdude

SOURCEDIR=`dirname $(realpath $0)`

MCU=$1
PROGRAMMER=$2

if [ -z "$MCU" ]; then
    echo 'no MCU'
    exit 1
fi
  
if [ -z $PROGRAMMER ]; then
    echo 'no programmer given'
    exit 1
fi


cd "$SOURCEDIR"

if [ "$MCU" = 'm8' ]; then
    BOARD="atmega8"
elif [ "$MCU" = 'm168p' ]; then
    BOARD="atmega168p"
else
    echo "Unknown microcontroller, cannot build! Use avrdude to find out the code:"
    echo "avrdude -c $PROCRAMMER -p m8"
    exit 1
fi

sh ./build.sh $BOARD

avrdude -c "$PROGRAMMER" -p "$MCU" -U "flash:w:build-$BOARD/jazda-$BOARD.hex"

