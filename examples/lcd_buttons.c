#define F_CPU 1000000UL

#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../src/display/pcd8544.c"

/* EXAMPLE: Switching the backlight
 * 
 * Plug in the MCU into the Jazda board.
 * 
 * After connecting power, the display will show which buttons are currently pressed.

Features: Pin change interrupt, pin input, pin output
*/


#ifdef ATMEGA8

#error "Atmega8 doesn't have pin change interrupts. Try the AtMega*8 series"

#endif

volatile uint8_t latest_buttons = 0;

uint8_t read_buttons(void) {
  // Assume all buttons share the same port
  return LEFTPORT & (_BV(LEFTPIN) | _BV(RIGHTPIN) | _BV(SELECTPIN) | _BV(BACKLIGHTBTNPIN));
}

void draw_marker(uint8_t filled, uint8_t button) {
  set_row(5);
  set_column(button * 21);
  for (uint8_t i = 0; i < 8; i++) {
    send_raw_byte(filled ? 0b11111111 : 0b00000011, true);
  }
}

ISR(PCINT2_vect) {
  latest_buttons = read_buttons();
}

int main(void) {
  LEFTDIR &= ~(1<<LEFTPIN);
  LEFTPULLUP |= 1<<LEFTPIN;

  RIGHTDIR &= ~(1<<RIGHTPIN);
  RIGHTPULLUP |= 1<<RIGHTPIN;
  
  SELECTDIR &= ~(1<<SELECTPIN);
  SELECTPULLUP |= 1<<SELECTPIN;
  
  BACKLIGHTBTNDIR &= ~(1<<BACKLIGHTBTNPIN);
  BACKLIGHTBTNPULLUP |= 1<<BACKLIGHTBTNPIN;

  PCMSK2 |= _BV(LEFTINT) | _BV(RIGHTINT) | _BV(BACKLIGHTBTNINT) | _BV(SELECTINT);
  
  PCICR |= 1<<PCIE2; // interrupt on both edges
  
  lcd_setup();
  lcd_init();
  latest_buttons = read_buttons();
  
  sei();

  while(1) {
    draw_marker(latest_buttons & _BV(LEFTPIN), 0);
    draw_marker(latest_buttons & _BV(SELECTPIN), 1);
    draw_marker(latest_buttons & _BV(RIGHTPIN), 2);
    draw_marker(latest_buttons & _BV(BACKLIGHTBTNPIN), 3);
  }
}
