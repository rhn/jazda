#define F_CPU 1000000UL
#include "../src/boards/v0.9.1.h"

#include <avr/io.h>

#include "../src/display/pcd8544.c"
#include "../src/display/drawing.c"

/* EXAMPLE 6: LCD countdown
 * 
 * Plug in the MCU into the Jazda board. The counter on the screen should start counting down.

Features: Serial output, number display
*/

int main(void) {
    lcd_setup();
    lcd_init();
    upoint_t position;
    position.x = 0;
    position.y = 0;
    upoint_t glyph_size;
    glyph_size.x = 10;
    glyph_size.y = 16;
    number_display_t number_data;
    number_data.integer = 3;
    number_data.fractional = 0;
    
    for (unsigned i = 180;; i--) {
        print_number(i/8, position, glyph_size, 2, number_data);
    }
}

