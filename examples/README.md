Examples
========

The examples are meant to provide a ramp for learning the codebase, to troubleshoot the device, or to add support for another microcontroller or peripheral.

They are structured to start from the simplest, and gradually go up in complexity. Some examples are meant to be run on the bike computer board.

Before starting, read the `HACKING.md` document, to set up *tup*. Then, build the examples:

```
tup build-atmega8/examples
```

To flash an example:

```
avrdude -c usbasp -p m168p -U "flash:w:build-atmega168p/examples/blink-atmega168p.hex"
```

Each example has instructions on connecting and the outcome inside. The progression is as follows:

1. `blink.c`: blinking light
2. `button.c`: simple button input
3. `buttonint.c`: interrupt-driven button input
4. `timer.c`: interrupt-driven timer
5. `lcd.c`: lcd test pattern
6. `lcd_counter.c`: countdown
7. `lcd_button.c`: button press count
8. `lcd_battery.c`: display current supply voltage

