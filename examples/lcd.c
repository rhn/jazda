#define F_CPU 1000000UL

#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#include "../src/display/pcd8544.c"

/* EXAMPLE: LCD test pattern
 * 
 * Plug in the MCU into the Jazda board. After connecting power, LCD should display a fractal pattern.

Features: Serial output
*/

int main(void) {
    uint8_t stamp = 0; // the actual value at start doesn't matter
    lcd_setup();
    lcd_init();
    for (;;stamp+=7) {
        _delay_ms(20);
        send_raw_byte(stamp, true);
    }
}

