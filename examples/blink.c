#define F_CPU 1000000UL  /* 1 MHz CPU clock */

#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <util/delay.h>

/* EXAMPLE 1: Blinking backlight
 *
 * Plug in the MCU into the Jazda board. After connecting power, the backlight should start flashing.
 * 
 * No inputs, no interrupts.
*/

void main(void) {
    BACKLIGHTDIR |= 1<<BACKLIGHTPIN; /* set pin to output */
         
    while (1) {
        /* set light pin to 1 (digital high) and delay */
        BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
        _delay_ms(200);
        
        /*  set light pin to off, and delay for 500ms */
        BACKLIGHTPORT &= ~_BV(BACKLIGHTPIN);
        _delay_ms(500);
    }
}
