#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <avr/interrupt.h>

/* EXAMPLE 3: Switching the backlight
 * 
 * Plug in the MCU into the Jazda board. Plug a button into the Wheel connector.
 * 
 * After connecting power, the backlight will turn on. Press the button to switch it.

Features: Pin change interrupt, pin input, pin output
*/

//#if (PULSEPORT != PORTD)//(PULSEPIN != PD2)// || 
  //#error "Pulse pin is not PD2. Example uses INT0 for interrupts, which is connected to PD2."
//#endif



#ifdef ATMEGA8

#define EINT_REG GICR
#define EINTC_REG MCUCR

#elif defined(ATMEGAX8)

#define EINT_REG EIMSK
#define EINTC_REG EICRA

#else
#error "Unsupported AtMega type"
#endif

/// Anything accessed from both interrupts and outside of them must be volatile
volatile int shining = 0;

ISR(INT0_vect) {
  if (shining)
  {
    BACKLIGHTPORT &= ~_BV(BACKLIGHTPIN);
    shining = 0;
  } else {
    BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
    shining = 1;
  }
}

int main(void) {
  BACKLIGHTDIR |= 1<<BACKLIGHTPIN;
  BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
  shining = 1;

  PULSEDIR &= ~_BV(PULSEPIN);
  PULSEPULLUP |= 1<<PULSEPIN;

  // Set Pin 6 (PD2) as the pin to use for interrupts
  // XXX isn't this valid only for tiny2313?
  // PCMSK |= (1<<PIND2);
  
  // interrupt on INT0 pin falling edge (sensor triggered)
  EINTC_REG |= (1<<ISC01);
  EINTC_REG &= ~(1<<ISC00);
  
  // turn on interrupts!
  EINT_REG |= (1<<INT0); // also known as GIMSK

  sei();

  while(1) {}
}

