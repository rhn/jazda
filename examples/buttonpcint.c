#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <avr/interrupt.h>

/* EXAMPLE: Switching the backlight
 * 
 * Plug in the MCU into the Jazda board.
 * 
 * After connecting power, the backlight will turn on. Press any buttons to switch it.

Features: Pin change interrupt, pin output
*/


#ifdef ATMEGA8

#error "Atmega8 doesn't have pin change interrupts. Try the AtMega*8 series"

#endif

/// Anything accessed from both interrupts and outside of them must be volatile
volatile int shining = 0;

ISR(PCINT2_vect) {
  if (shining)
  {
    BACKLIGHTPORT &= ~_BV(BACKLIGHTPIN);
    shining = 0;
  } else {
    BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
    shining = 1;
  }
}

int main(void) {
  BACKLIGHTDIR |= 1<<BACKLIGHTPIN;
  BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
  shining = 1;

  LEFTDIR &= ~(1<<LEFTPIN);
  LEFTPULLUP |= 1<<LEFTPIN;

  RIGHTDIR &= ~(1<<RIGHTPIN);
  RIGHTPULLUP |= 1<<RIGHTPIN;
  
  SELECTDIR &= ~(1<<SELECTPIN);
  SELECTPULLUP |= 1<<SELECTPIN;
  
  BACKLIGHTBTNDIR &= ~(1<<BACKLIGHTBTNPIN);
  BACKLIGHTBTNPULLUP |= 1<<BACKLIGHTBTNPIN;

  PCMSK2 |= _BV(LEFTINT) | _BV(RIGHTINT) | _BV(BACKLIGHTBTNINT) | _BV(SELECTINT);
  
  // turn on interrupts!
  PCICR |= 1<<PCIE2; // interrupt on both edges
  sei();

  while(1) {}
}

