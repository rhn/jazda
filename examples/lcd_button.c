#define F_CPU 1000000UL

#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <avr/interrupt.h>

#include "../src/display/pcd8544.c"
#include "../src/display/drawing.c"

/* EXAMPLE: LCD button press counter
 * 
 * Plug in the MCU into the Jazda board. Plug in a button into the Wheel connector.
 * 
 * The counter on the screen should count the number of button presses.

Features: Serial output, hardware interrupr, number display, idle mode
*/


#ifdef ATMEGA8

#define EINT_REG GICR
#define EINTC_REG MCUCR

#elif defined(ATMEGAX8)

#define EINT_REG EIMSK
#define EINTC_REG EICRA

#else
#error "Unsupported AtMega type"
#endif


volatile uint8_t press_count = 0;

ISR(INT0_vect) {
  press_count += 1;
}

int main(void) {
    PULSEDIR &= ~_BV(PULSEPIN);
  PULSEPULLUP |= 1<<PULSEPIN;
  
  EINTC_REG |= (1<<ISC01);
  EINTC_REG &= ~(1<<ISC00);

  EINT_REG |= (1<<INT0);

  sei();

    lcd_setup();
    lcd_init();
    upoint_t position;
    position.x = 0;
    position.y = 0;
    upoint_t glyph_size;
    glyph_size.x = 10;
    glyph_size.y = 16;
    number_display_t number_data;
    number_data.integer = 3;
    number_data.fractional = 0;
    
    while(1) {
        print_number(press_count, position, glyph_size, 2, number_data);
    }
}

