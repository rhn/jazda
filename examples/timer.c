#include "../src/boards/v0.9.1.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>

/* EXAMPLE: Timer

Interrupt-driven blinking
* 
 * Plug in the MCU into the Jazda board. After connecting power, the backlight should start flashing: 1 second off, a short time on.

Features: Timer overflow interrupt, timer trigger interrupt, pin output
*/

#ifdef ATMEGA8

#define TIMER_REG TIMSK

#elif defined(ATMEGAX8)

#define TIMER_REG TIMSK1

#else
#error "Unsupported AtMega type"
#endif

volatile uint16_t triggers = 0;

void handle_int(void) {
  if (triggers % 2) {
    BACKLIGHTPORT &= ~_BV(BACKLIGHTPIN);
  } else {
    BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
  }
  triggers++;
}

ISR(TIMER1_OVF_vect) {
  handle_int();
}

ISR(TIMER1_COMPB_vect) {
  handle_int();
}


int main(void) {
  BACKLIGHTDIR |= 1<<BACKLIGHTPIN;
  BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
  
  // set prescaler for 16bit clock to 0b010 for clk/8
  // 16× more ticks than jazda, for an overflow just over every second at 1MHz IO clock
  // FIXME: in reality, it's slightly faster than 2 times per second
  uint8_t clksrc = TCCR1B;
  //clksrc |= 1<<CS12 | 1<<CS11 | 0<<CS10;
  //clksrc &= ~(0<<CS12 | 0<<CS11 | 1<<CS10);
  clksrc |= 0<<CS12 | 1<<CS11 | 0<<CS10;
  clksrc &= ~(1<<CS12 | 0<<CS11 | 1<<CS10);
  TCCR1B = clksrc;

  TIMER_REG |= _BV(TOIE1); // Set overflow interrupt

  // Set up comparator interrupt
  OCR1B = TCNT1 + ((uint16_t)ONE_SECOND * 16);
  TIMER_REG |= 1 << OCIE1B;

  sei();
  while (1) {}
}

