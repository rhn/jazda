#include "../src/boards/v0.9.1.h"

#include <avr/io.h>

/** EXAMPLE 2: Switching diodes

 * Plug in the MCU into the Jazda board.
 * 
 * After connecting power, the backlight should light up. Hold the backlight (rightmost) button to keep it dim.

Features: Pin input, pin output
*/

int main(void) {
  BACKLIGHTDIR |= 1<<BACKLIGHTPIN;

  BACKLIGHTBTNDIR &= ~_BV(BACKLIGHTBTNPIN); /* set pin to input */
  // To protect from badly adjusted resistors on the PCB, the pullup is enabled. Its value is 20-50kΩ.
  BACKLIGHTBTNPULLUP |= 1<<BACKLIGHTBTNPIN; /* enable pullup resistor */

  while(1) {
    if (BACKLIGHTBTNPORT & 1<<BACKLIGHTBTNPIN) { // check pin value
      BACKLIGHTPORT |= _BV(BACKLIGHTPIN);
    } else {
      BACKLIGHTPORT &= ~_BV(BACKLIGHTPIN);
    }
  }
}

