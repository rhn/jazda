#define F_CPU 1000000UL
#include "../src/boards/v0.9.1.h"

#include <avr/io.h>

#include "../src/display/pcd8544.c"
#include "../src/display/drawing.c"


/* EXAMPLE: ADC used to measure battery voltage
 * 
 * Plug in the MCU into the Jazda board. The display should show the current supply voltage.

Features: Serial output, analog comparator, number printing.
*/

// TODO: include avr support library
#define ADC_CHAN_V_BG (-2)

#ifdef ATMEGA8

#define V_BG_cV 130 // Centivolts of Vbg

#elif defined(ATMEGAX8)

#define V_BG_cV 110 // Centivolts of Vbg

#else
#error "Unsupported AtMega type"
#endif

/* initializes adc with no interrupts */
void adc_init(void) {
    // use Vcc as reference
    ADMUX |= (1 << REFS0);
    ADMUX &= ~(1 << REFS1);
    // select Vbg as voltage
    ADMUX |= (1 << MUX3) | (1 << MUX2) | (1 << MUX1);
    ADMUX &= ~(1 << MUX0);
}

void adc_enable(void) {
    ADCSRA |= (1 << ADEN);
}

void adc_disable(void) {
    ADCSRA &= ~(1 << ADEN);
}

uint16_t adc_read(int8_t channel) {
    uint8_t mux_bits = 0xf; // 4 bits select the channel
    uint8_t channel_masked = ((uint8_t)channel) & mux_bits; // writing unnormalized value could clobber other bits in the register

    // Request a measurement
    ADMUX |= channel_masked << MUX0;
    ADCSRA |= (1 << ADSC);

    // Wait for measurement
    while (ADCSRA & (1<<ADSC)) {}

    return ADC;
}

/* blocking voltage measurement. returns reading in units of 10mV. Resolution: 40mV.
TODO: think about better returned units */
uint16_t get_voltage(void) {
    uint16_t raw_reading = adc_read(ADC_CHAN_V_BG); // -2 == Vbg source @ 1.3V

    // R - reading
    // V_{BG} - precalibrated source
    // V_{CC} - battery, reference
    // R = {V_{BG}\cdot1024}\over{V_{CC}}
    // V_{CC} = {V_{BG}\cdot1024}\overR
    if (raw_reading == 0) {
        return 0;
    }
    
    uint16_t voltage = ((V_BG_cV << 8) / raw_reading) << 2;
    return voltage;
}

int main(void) {
    upoint_t position;
    upoint_t glyph_size;
    number_display_t number_display;
    number_display.integer = 1;
    number_display.fractional = 2;
    position.x = 1;
    glyph_size.x = 8;
    glyph_size.y = 8;
    lcd_setup();
    lcd_init();
    adc_init();
    adc_enable();
    // clearing a portion of the screen
    lcd_clean();
    for (;;) {
        uint16_t voltage = get_voltage();
        // printing digits to screen. voltage is a fraction of Vcc
        position.y = 2;
        print_number(voltage, position, glyph_size, 1, number_display);        
        send_raw_byte(0, true);
        _delay_ms(200);
    }
}
