#!/bin/sh
set -e

# Builds jazda for a selected build variant

SOURCEDIR=`dirname $(realpath $0)`

VARIANT=$1
PROGRAMMER=$2

if [ -z "$VARIANT" ]; then
    echo 'no variant given'
    exit 1
fi

cd "$SOURCEDIR"

if [ ! -d .tup ]; then
    tup init
fi

if [ ! -d "build-$VARIANT" ]; then
    tup variant "config/$VARIANT.config"
fi

tup "build-$VARIANT/jazda-$VARIANT.hex"
